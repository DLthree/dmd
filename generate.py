#!/usr/bin/python

import dmd
import string
import os
import shutil

def generate(outdir, template, item_file, iterations=1, **kwargs):
    items = parse_file(item_file)
    print "Cleaning %s" % outdir
    shutil.rmtree(outdir, ignore_errors=True)
    os.makedirs(outdir)
    generator = dmd.CardGenerator(outdir, template, **kwargs)

    for item in items:
        generator.add(**item)        

    images = generator.generate_images()
    print "Generated %s" % string.join(images)

    pages = generator.generate_pages(images, iterations=iterations)
    print "Generated %s" % string.join(pages)

def read_file(filename):
    f = open(filename, "r")
    data = f.read()
    f.close()
    return data

def parse_file(filename):
    retval = []
    data = read_file(filename)
    entries = data.split("================================================================================")
    for entry in entries[1:]:
        item = { }
        lines = entry.split("\n")
        for line in lines:
            line = line.strip()
            if not line: continue
            vals = line.split("=")
            if len(vals) != 2:
                print "Ignored line: %s" % line
                continue
            left,right = vals
            right = right.replace(r"\n","\n")
            item[left] = right
        retval.append(item)
    return retval


if __name__ == "__main__":
    generate(outdir="output/wounds", template="templates/wounds.svg", item_file="data/Wounds Deck.txt", iterations=4, tile_width=3, tile_height=11)

    raise Exception("foolish boy")
    generate(outdir="output/monsters", template="templates/multiline.svg", item_file="data/Monsters Deck.txt")
    generate(outdir="output/encounters", template="templates/abilities-front.svg", item_file="data/Encounters Deck.txt")
    generate(outdir="output/modifiers", template="templates/multiline.svg", item_file="data/Modifiers Deck.txt")
    generate(outdir="output/races", template="templates/multiline.svg", item_file="data/Race Deck.txt")
    generate(outdir="output/lootsies", template="templates/abilities-front.svg", item_file="data/Loot Deck.txt")
    generate(outdir="output/backs", template="templates/abilities-front.svg", item_file="data/Abilities Deck Back.txt")
    generate(outdir="output/abilities", template="templates/abilities-front.svg", item_file="data/Abilities Deck.txt")
    
