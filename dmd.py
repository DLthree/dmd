#!/usr/bin/python

import os
import string

# change this to your path for inkscape
inkscape = "/Applications/Inkscape.app/Contents/Resources/bin/inkscape"

# location of ImageMagick montage
montage = "montage"

class CardGenerator(object):
    def __init__(self, out_dir, template, tile_width=3, tile_height=3):
        try:
            os.mkdir(out_dir)
        except:
            pass 
        self.out_dir = out_dir
        self.template = template
        self.cards = []
        self.tile_width = tile_width
        self.tile_height = tile_height

    def add(self, **kwargs):
        self.cards.append(kwargs)
        
    def generate_images(self):
        names = []
        for card in self.cards:
            filename = "%s/%s %s.png" % (self.out_dir, card['category'], card['name'])
            self._generate_image(filename, card)
            names.append(filename)
        return names            

    def _generate_image(self, filename, card):
        repl = {}
        for key, val in card.iteritems():
            repl[key.upper()] = val
        self.template_to_png(self.template, filename, repl)
        return filename

    def generate_pages(self, images, iterations=1):
        return self.tile_all(images*iterations, self.out_dir, tile_width=self.tile_width, tile_height=self.tile_height)

    @staticmethod
    def instantiate(template_filename, output_filename, replace_dict):
        fin = open(template_filename, "r")
        data = fin.read()
        fin.close()

        for key, val in replace_dict.iteritems():
            data = data.replace("%%%%%s%%%%" % key, val)

        fout = open(output_filename, "w")
        fout.write(data)
        fout.close()

    @staticmethod
    def png_export(svg_filename, png_filename):
        CardGenerator.cmd('"%s" "%s" -e "%s"' % (inkscape, svg_filename, png_filename))

    @staticmethod
    def template_to_png(template, output, replacements):
        tmp = "%s.svg" % os.tmpnam()
        CardGenerator.instantiate(template, tmp, replacements)
        CardGenerator.png_export(tmp, output)
        os.unlink(tmp)

    @staticmethod
    def tile(images, output, tile_width, tile_height):
        quoted_names = map(lambda x: '"%s"' % x, images)
        files = string.join(quoted_names)
        CardGenerator.cmd('"%s" %s -frame 2 -tile %dx%d -geometry +1+1 "%s"' % (montage, files, tile_width, tile_height, output))

    @staticmethod
    def cmd(s):
        print "Executing:",s
        os.system(s)

    @staticmethod
    def split(l, index):
        return l[:index], l[index:]

    @staticmethod
    def tile_all(images, directory, tile_width, tile_height):
        names = []
        i = 0
        while images:
            curr, rest = CardGenerator.split(images, tile_width*tile_height)
            filename = "%s/page%d.png" % (directory, i)
            CardGenerator.tile(curr, filename, tile_width, tile_height)
            names.append(filename)
            images = rest
            i += 1
        return names
        

if __name__ == "__main__":
    # Sample usage:
    
    START_GREEN = '<flowSpan style="fill:#00ff00">'
    END_GREEN = '</flowSpan>'
    START_RED = '<flowSpan style="fill:#ff0000">'
    END_RED = '</flowSpan>'

    role_front = CardGenerator(out_dir="role_front", template="dmd-front-role-template.svg")
    
    role_front.add(category="Support Magic",
                   name="Haste",
                   description="Once per day, before combat starts, play this card with an %sArcane%s Wound card to give you and each ally the chance to use their basic attack twice this combat" % (START_GREEN, END_GREEN))
    
    role_front.add(category="Offensive Magic",
                   name="Flare",
                   description="Once per day, at speed %s6%s, play this card with as many Fire Wound cards as you wish.  Deal that many Fire Wounds to one enemy." % (START_RED, END_RED))

    images = role_front.generate_images()
    print "Generated %s" % string.join(images)

    pages = role_front.generate_pages(images)
    print "Generated %s" % string.join(pages)
    
    
    


