Mechanics:

Turns are taken in Speed order, and each player can draft one Wound card from the offering for their use. (Multiple cards?)
(Where do monsters get their wound cards from?)

Race provides basic attack (Orcs start with "You may play one Slashing Wound", and an initiative value)

Orc: 2 Wounds, 3 Speed, you may play one Slashing Wound as an attack.

Ability cards can be played face-up for their normal ability (which usually amounts to attaching to a wound card and increasing its value), or face-down for their universal ability. (Melee Combat: you may play this card in place of one Slashing, Blunt, or Piercing Wound)


Training Sword: If you level up while wielding this sword, gain a Melee Combat level in addition to your normal level. (Cannot exceed max ranks)

Melee Combat

Weapon Mastery: You can attack for 2 Piercing, Slashing, or Blunt damage at initiative 3.

Weapon Mastery: You may spend a Piercing, Slashing, or Blunt Power as 2 damage of that type at initiative 3.
Level 2: You may spend a Piercing, Slashing, or Blunt Power as 3 damage of that type at initiative 3.
Level 3: You may spend a Piercing, Slashing, or Blunt Power as 4 damage of that type at initiative 3.

- Backstab (Pierce)

  Unpowered: Play this when you make a melee combat attack. The attack is   made at initiative 7.

  Powered: Play this when you make a combat attack. If the attack is from   a card, you get the powered version for free, and if the attack is made
  at higher initiative than its target, you deal 2 extra piercing damage    on that attack.

- Deep Cut (Slash)

  Unpowered: Deal 1 slashing damage (Init 4).

  Powered: Deal 2 slashing damage (Init 6).

- Crushing Blow (Blunt)

  Unpowered: Deal 1 blunt damage (Init 4).

  Powered: Deal 3 blunt damage (Init 2).

- Cleave (Slash)

  Unpowered: Deal 1 slashing damage (Init 1) to all enemies.

  Powered: Deal 2 slashing damage (Init 6).

-  (Blunt)

  Unpowered: Deal 1 blunt damage (Init 4).

  Powered: 


================================================================================

category=Melee Combat
name=Weapon Master
description=You may play a Slashing, Blunt, or Piercing Wound card as an attack at Speed 4, in addition to your other attacks.
================================================================================
category=Melee Combat
name=Double Attack
description=Play this card with a Slashing, Blunt, or Piercing attack. When you do, you may play another Wound card of the same type on the same or a different Monster.
================================================================================
category=Melee Combat
name=Power Attack
description=Play this card with a Slashing or Blunt attack. The attack does double damage, but is delayed until Speed 1.
================================================================================
category=Melee Combat
name=Cleave
description=Play this card with a Slashing or Blunt attack, and place the Wound between two monsters. The attack hits both of them. 
================================================================================
category=Melee Combat
name=Backstab
description=Play this card with a Piercing attack. If the target has not acted yet, the Wound does double damage.
================================================================================
category=Ranged Combat
name=Thrown Hammer
description=You may treat your Piercing attacks as Blunt attacks this turn, for the purpose of resistances and immunities.
================================================================================
category=Ranged Combat
name=Shuriken
description=You may treat your Piercing attacks as Slashing attacks this turn, for the purpose of resistances and immunities.
================================================================================
category=Ranged Combat
name=Distance Shot
description=All of your Piercing attacks take place at speed 9 this turn.
================================================================================
category=Ranged Combat
name=Reload
description=You may take up to 3 Piercing Wound cards from the offer and place them under this card. In future combats, you may use Piercing Wound cards under this card as if they were in the wound offer. After use, each card is returned to the Wound discard pile. This card can store a maximum of 3 Wounds at once.   
================================================================================
category=Ranged Combat
name=Aimed Shot
description=Play this card with a Piercing attack. It deals double damage, but its speed it reduced by 2.
================================================================================
category=Defense Combat
name=Sword Guard
description=Play this card when attacked to gain Slashing resistance this turn.
================================================================================
category=Defense Combat
name=Arrow Guard
description=Play this card when attacked to gain Piercing resistance this turn.
================================================================================
category=Defense Combat
name=Mace Guard
description=Play this card when attacked to gain Blunt resistance this turn.
================================================================================
category=Defense Combat
name=Shield Bash
description=Whenever a monster deals damage to you this turn, you may play one Blunt Wound card as an attack on them.
================================================================================
category=Defense Combat
name=Spell Reflect
description=Play this card when a monster deals Fire, Ice, Arcane, or Divine damage to you. Redirect one Wound from the attack to a monster of your choice.
================================================================================
category=Healing Magic
name=Cauterize 
description=Once per day, play this card on yourself or an ally who has taken a Fire Wound and a Slashing Wound. Remove both Wounds. 
================================================================================
category=Healing Magic
name=Numbness
description=Once per day, play this card on yourself or an ally who has taken a Cold Wound and a Blunt Wound. Remove both Wounds. 
================================================================================
category=Healing Magic
name=Null Magic
description=Once per day, play this card on yourself and all allies. You may play a Fire Wound card to negate an existing Cold Wound card on a target (or vice-versa), and you may play an Arcane Wound card to negate an existing Divine Wound card on a target (or vice-versa.) You may do this any number of times this turn.
================================================================================
category=Healing Magic
name=Turn Undead
description=Once per day, before combat starts, play this card with an Divine Wound card on an undead monster to deal that Divine Wound to it as double damage and prevent the monster from attacking.
================================================================================
category=Healing Magic
name=Lay on Hands
description=Once per day, you may play this card with any Wound Card to remove one Wound of that kind from yourself and all allies. 
================================================================================
category=Support Magic
name=Enchant
description=Once per day, before combat starts, play this card with a Fire or Cold Wound card on yourself or an ally. For the duration of this combat, whenever the target attacks, they deal an additional Wound of the type used in the Enchant. 
================================================================================
category=Support Magic
name=Slow
description=Once per day, before combat starts, play this card with an Arcane Wound card to set all monster's speeds to 1.
================================================================================
name=Haste
category=Support Magic
description=Once per day, before combat starts, play this card with an Arcane Wound card to give you and each ally the chance to use their basic attack twice this combat. 
================================================================================
category=Support Magic
name=Protection from Arrows
description=Once per day, before combat starts, play this card with an Arcane Wound card to prevent all Piercing Wounds from monsters for the duration of combat.
================================================================================
category=Support Magic
name=Shatter Armor
description=Once per day, before combat starts, play this card with a Blunt Wound card on an enemy to remove all resistances and deal a Blunt Wound to them.
================================================================================
category=Offensive Magic
name=Flare
description=Once per day, at speed 6, play this card with as many Fire Wound cards as you wish. Deal that many Fire Wounds to one enemy.
================================================================================
category=Offensive Magic
name=Fireball
description=Once per day, at speed 6, play this card with as many Fire Wound cards as you wish. Deal one Fire Wound to that many enemies, or deal two Fire Wounds to half that many enemies (round down).
================================================================================
category=Offensive Magic
name=Freeze
description=Once per day, at speed 8, play this card with as many Ice Wound cards as you wish. Deal one Ice Wound to one enemy and reduce his speed by as many Ice Wounds as were played. If reduced to speed 0, the target does not attack this combat.
================================================================================
category=Offensive Magic
name=Ice Storm
description=Once per day, at speed 4, play this card with two Ice Wound cards. Deal one Ice Wound to all enemies and remove their blunt resistance. 
================================================================================
category=Offensive Magic
name=Magic Missile
description=Once per day, at speed 10, play this card with an Arcane Wound card to deal one Arcane Wound to one enemy. You may play an Arcane Wound card to untap this card at any time, even on subsequent combats.
================================================================================
category=Skills
name=Endurance
description=Once per day, prevent the damage to yourself from one attack.
================================================================================
category=Skills
name=Concentration
description=Once per day, untap one tapped card of yourself or an ally.
================================================================================
category=Skills
name=Perception
description=You may look at the next encounter card. You can play this at any time and as often as you like.
================================================================================
category=Skills
name=Diplomacy
description=Once per day, play this card on a monster. You get experience and loot from that monster, even if it is not killed.
================================================================================
category=Skills
name=Lockpicking
description=Whenever you recieve loot, you may draw an additional card from the loot deck and discard one of the drawn cards of your choice.
================================================================================
category=Skills
name=Stealth
description=Once per day, play this card before combat starts. You act at Speed 10 this combat.

