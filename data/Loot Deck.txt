(2)Weapon
(3)Armor
(2)Accessory
(3)Consumable

================================================================================
name=Magic Armor +1
category=Armor
description=+1 Health (stacks up to 5 times)
================================================================================
name=Magic Armor +1
category=Armor
description=+1 Health (stacks up to 5 times)
================================================================================
name=Magic Armor +1
category=Armor
description=+1 Health (stacks up to 5 times)
================================================================================
name=Sunblade
category=Weapon
description=Can spend a Fire or Diving Wound card with your basic attack.
================================================================================
name=Sleep Arrow
category=Weapon
description=Play with a Piercing attack. The target does not attack.
================================================================================
name=Quicksilver Ring
category=Accessory
description=+1 Speed
================================================================================
name=Fire Cloak
category=Acessory
description=Fire Resistance
================================================================================
name=Potion of Healing
category=Consumable
description=Remove all wounds from one hero
================================================================================
name=Tent
category=Consumable
description=Sleep instead of the next encounter
================================================================================
name=Potion of Bull's Strength
category=Consumable
description=All physical attacks deal double damage this combat

